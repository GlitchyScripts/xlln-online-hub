#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <vector>
#include <stdarg.h>
#include <netdb.h>
#include "utils.hpp"

// new'ly[] allocate a copy of this input string.
char* CloneString(const char* str)
{
	if (!str) {
		char* resultEmpty = new char[1];
		resultEmpty[0] = 0;
		return resultEmpty;
	}
	size_t strBufLen = strlen(str) + 1;
	char* result = new char[strBufLen];
	memcpy(result, str, strBufLen);
	return result;
}

char* FormMallocString(const char* const format, ...)
{
	size_t currentLen = 1;
	size_t currentBufLen = sizeof(char) * currentLen;
	char* resultStr = (char*)malloc(currentBufLen);

	va_list args;
	while (1) {
		va_start(args, format);
		int needLen = vsnprintf(resultStr, currentLen, format, args);
		va_end(args);
		if (needLen < 0) {
			// string formatting error.
			free(resultStr);
			return 0;
		}
		if ((size_t)needLen >= currentLen) {
			currentLen = needLen + 1;
			currentBufLen = sizeof(char) * currentLen;
			resultStr = (char*)realloc(resultStr, currentBufLen);
		}
		else {
			break;
		}
	}
	return resultStr;
}

uint16_t GetSockAddrPort(const sockaddr_storage *sockAddrStorage)
{
	uint16_t portHBO =
		sockAddrStorage->ss_family == AF_INET
		? ntohs((*(sockaddr_in*)sockAddrStorage).sin_port)
		: (
			sockAddrStorage->ss_family == AF_INET6
			? ntohs((*(sockaddr_in6*)sockAddrStorage).sin6_port)
			: 0
			);
	return portHBO;
}

bool SetSockAddrPort(sockaddr_storage *sockAddrStorage, uint16_t portHBO)
{
	switch (sockAddrStorage->ss_family) {
	case AF_INET:
		(*(sockaddr_in*)sockAddrStorage).sin_port = htons(portHBO);
		break;
	case AF_INET6:
		(*(sockaddr_in6*)sockAddrStorage).sin6_port = htons(portHBO);
		break;
	default:
		return false;
	}
	return true;
}

/// Malloc'd result.
char* GetSockAddrInfo(const sockaddr_storage *sockAddrStorage)
{
	// Maximum length of full domain name + sentinel.
	char namebuf[253 + 1];
	int error = getnameinfo((sockaddr*)sockAddrStorage, sizeof(sockaddr_storage), namebuf, sizeof(namebuf), NULL, 0, NI_NUMERICHOST);
	if (!error) {
		uint16_t portHBO = GetSockAddrPort(sockAddrStorage);
		return portHBO
			? (
				sockAddrStorage->ss_family == AF_INET
				? FormMallocString("%s:%hu", namebuf, portHBO)
				: FormMallocString("[%s]:%hu", namebuf, portHBO)
			)
			: FormMallocString("%s", namebuf);
	}
	return 0;
}

bool SockAddrsMatch(const sockaddr_storage* sockAddr1, const sockaddr_storage* sockAddr2)
{
	return SockAddrsMatch(sockAddr1, sockAddr2, false);
}

bool SockAddrsMatch(const sockaddr_storage* sockAddr1, const sockaddr_storage* sockAddr2, bool only_family_and_address)
{
	if (sockAddr1->ss_family != sockAddr2->ss_family) {
		return false;
	}
	
	switch (sockAddr1->ss_family) {
		case AF_INET: {
			if (
				((sockaddr_in*)sockAddr1)->sin_addr.s_addr != ((sockaddr_in*)sockAddr2)->sin_addr.s_addr
				|| (
					!only_family_and_address
					&& ((sockaddr_in*)sockAddr1)->sin_port != ((sockaddr_in*)sockAddr2)->sin_port
				)
			) {
				return false;
			}
			break;
		}
		case AF_INET6: {
			if (memcmp(&((sockaddr_in6*)sockAddr1)->sin6_addr.s6_addr, &((sockaddr_in6*)sockAddr2)->sin6_addr.s6_addr, sizeof(in6_addr)) != 0) {
				return false;
			}
			if (
				!only_family_and_address
				&& (
					((sockaddr_in6*)sockAddr1)->sin6_port != ((sockaddr_in6*)sockAddr2)->sin6_port
					|| ((sockaddr_in6*)sockAddr1)->sin6_flowinfo != ((sockaddr_in6*)sockAddr2)->sin6_flowinfo
					|| ((sockaddr_in6*)sockAddr1)->sin6_scope_id != ((sockaddr_in6*)sockAddr2)->sin6_scope_id
				)
			) {
				return false;
			}
			break;
		}
		default: {
			if (memcmp(sockAddr1, sockAddr2, sizeof(sockaddr_storage)) != 0) {
				return false;
			}
			break;
		}
	}
	
	return true;
}
