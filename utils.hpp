#pragma once

char* CloneString(const char* str);
char* FormMallocString(const char* const format, ...);
uint16_t GetSockAddrPort(const sockaddr_storage *sockAddrStorage);
bool SetSockAddrPort(sockaddr_storage *sockAddrStorage, uint16_t portHBO);
char* GetSockAddrInfo(const sockaddr_storage *sockAddrStorage);
bool SockAddrsMatch(const sockaddr_storage* sockAddr1, const sockaddr_storage* sockAddr2);
bool SockAddrsMatch(const sockaddr_storage* sockAddr1, const sockaddr_storage* sockAddr2, bool only_family_and_address);
