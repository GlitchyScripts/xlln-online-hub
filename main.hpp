#pragma once
#include <stdint.h>
#include <arpa/inet.h>

#define XLLN_DEBUG

// Version values must not exceed 0xFF.
#define VERSION_MAJOR       1
#define VERSION_MINOR       4
#define VERSION_REVISION    0
#define VERSION_BUILD       1

#define XLLN_LOG_LEVEL_MASK		(0b00111111)
// Function call tracing.
#define XLLN_LOG_LEVEL_TRACE	(0b00000001)
// Function, variable and operation logging.
#define XLLN_LOG_LEVEL_DEBUG	(0b00000010)
// Generally useful information to log (service start/stop, configuration assumptions, etc).
#define XLLN_LOG_LEVEL_INFO		(0b00000100)
// Anything that can potentially cause application oddities, but is being handled adequately.
#define XLLN_LOG_LEVEL_WARN		(0b00001000)
// Any error which is fatal to the operation, but not the service or application (can't open a required file, missing data, etc.).
#define XLLN_LOG_LEVEL_ERROR	(0b00010000)
// Errors that will terminate the application.
#define XLLN_LOG_LEVEL_FATAL	(0b00100000)

#define XLLN_LOG_CONTEXT_MASK			(0b11000111 << 8)
// Logs related to Xlive(GFWL) functionality.
#define XLLN_LOG_CONTEXT_XLIVE			(0b00000001 << 8)
// Logs related to XLiveLessNess functionality.
#define XLLN_LOG_CONTEXT_XLIVELESSNESS	(0b00000010 << 8)
// Logs related to XLLN-Module functionality.
#define XLLN_LOG_CONTEXT_XLLN_MODULE	(0b00000100 << 8)
// Logs related to functionality from other areas of the application.
#define XLLN_LOG_CONTEXT_OTHER			(0b10000000 << 8)
// Logs related to the Title functionality.
#define XLLN_LOG_CONTEXT_TITLE			(0b01000000 << 8)

#define IsUsingBasePort(base_port) (base_port != 0 && base_port != 0xFFFF)

uint32_t XLLNDebugLogF(uint32_t logLevel, const char *const format, ...);

#ifdef XLLN_DEBUG
#define XLLN_DEBUG_LOG(logLevel, format, ...) XLLNDebugLogF(logLevel, format, ## __VA_ARGS__)
#define GET_SOCKADDR_INFO(sockAddrStorage) GetSockAddrInfo(sockAddrStorage)
#else
#define XLLN_DEBUG_LOG(logLevel, format, ...)
#define GET_SOCKADDR_INFO(sockAddrStorage) NULL
#endif

#define SOCKADDR_STORAGE sockaddr_storage
#define XUID uint64_t

namespace XllnNetworkPacket {
	const char* const TYPE_NAMES[] {
		"UNKNOWN"
		, "TITLE_PACKET"
		, "TITLE_BROADCAST_PACKET"
		, "PACKET_FORWARDED"
		, "UNKNOWN_USER_ASK"
		, "UNKNOWN_USER_REPLY"
		, "CUSTOM_OTHER"
		, "LIVE_OVER_LAN_ADVERTISE"
		, "LIVE_OVER_LAN_UNADVERTISE"
		, "HUB_REQUEST"
		, "HUB_REPLY"
		, "QOS_REQUEST"
		, "QOS_RESPONSE"
		, "HUB_OUT_OF_BAND"
		, "HUB_RELAY"
		, "DIRECT_IP_REQUEST"
		, "DIRECT_IP_RESPONSE"
	};
	typedef enum : uint8_t {
		XLLN_NPT_UNKNOWN = 0
		, XLLN_NPT_FIRST
		
		, XLLN_NPT_TITLE_PACKET = XLLN_NPT_FIRST
		, XLLN_NPT_TITLE_BROADCAST_PACKET // Unused from XLLN v1.6.
		, XLLN_NPT_PACKET_FORWARDED
		, XLLN_NPT_UNKNOWN_USER_ASK // Unused from XLLN v1.6.
		, XLLN_NPT_UNKNOWN_USER_REPLY // Unused from XLLN v1.6.
		, XLLN_NPT_CUSTOM_OTHER // Unused.
		, XLLN_NPT_LIVE_OVER_LAN_ADVERTISE
		, XLLN_NPT_LIVE_OVER_LAN_UNADVERTISE
		, XLLN_NPT_HUB_REQUEST
		, XLLN_NPT_HUB_REPLY
		, XLLN_NPT_QOS_REQUEST
		, XLLN_NPT_QOS_RESPONSE
		, XLLN_NPT_HUB_OUT_OF_BAND // Unused from XLLN v1.6.
		, XLLN_NPT_HUB_RELAY // Unused from XLLN v1.6.
		, XLLN_NPT_DIRECT_IP_REQUEST
		, XLLN_NPT_DIRECT_IP_RESPONSE
		
		, XLLN_NPT_LAST = XLLN_NPT_DIRECT_IP_RESPONSE
	} TYPE;
	
	const char* GetPacketTypeName(XllnNetworkPacket::TYPE type);
	
#pragma pack(push, 1) // Save then set byte alignment setting.
	
	typedef struct _TITLE_PACKET {
		uint32_t instanceId = 0;
		uint16_t titlePortSource = 0; // Host Byte Order.
		uint16_t titlePortDestination = 0; // Host Byte Order.
	} TITLE_PACKET;
	
	typedef struct _PACKET_FORWARDED {
		SOCKADDR_STORAGE originSockAddr;
		// The data following this is the forwarded packet data.
	} PACKET_FORWARDED;
	
	typedef struct _HUB_REQUEST_PACKET {
		uint32_t xllnVersion = 0; // version of the requester.
		uint32_t instanceId = 0; // Instance ID of the requester.
		uint32_t titleId = 0;
		uint32_t titleVersion = 0;
		uint32_t portBaseHBO = 0;
	} HUB_REQUEST_PACKET;
	
	typedef struct _HUB_REPLY_PACKET {
		uint8_t isHubServer = 0; // boolean.
		uint32_t xllnVersion = 0; // version of the replier.
		uint32_t recommendedInstanceId = 0; // the Instance ID that should be used instead (in case of collisions).
	} HUB_REPLY_PACKET;
	
	typedef struct _LIVE_OVER_LAN_UNADVERTISE {
		uint32_t instanceId = 0;
		uint8_t sessionType = 0;
		XUID xuid = 0;
	} LIVE_OVER_LAN_UNADVERTISE;
	
	typedef struct _QOS_REQUEST {
		uint32_t qosLookupId = 0;
		uint64_t sessionId = 0; // XNKID
		uint32_t probeId = 0;
		uint32_t instanceId = 0; // Instance ID of the requester.
	} QOS_REQUEST;
	
	typedef struct _QOS_RESPONSE {
		uint32_t qosLookupId = 0;
		uint64_t sessionId = 0; // XNKID
		uint32_t probeId = 0;
		uint32_t instanceId = 0; // Instance ID of the responder.
		uint8_t enabled = 0;
		uint16_t sizeData = 0; // the amount of data appended to the end of this packet type.
	} QOS_RESPONSE;
	
	typedef struct _DIRECT_IP_REQUEST {
		uint32_t joinRequestSignature = 0;
		uint8_t passwordSha256[32];
	} DIRECT_IP_REQUEST;
	
	typedef struct _DIRECT_IP_RESPONSE {
		uint32_t joinRequestSignature = 0;
		uint32_t instanceId = 0;
		uint32_t titleId = 0;
	} DIRECT_IP_RESPONSE;
	
	// The following structs are unused from XLLN v1.6.
	
	typedef struct _NET_USER_PACKET {
		uint32_t instanceId = 0; // a generated UUID for that instance.
		uint16_t portBaseHBO = 0; // Base Port of the instance. Host Byte Order.
		uint16_t socketInternalPortHBO = 0;
		int16_t socketInternalPortOffsetHBO = 0;
		uint32_t instanceIdConsumeRemaining = 0; // the instanceId that should be consuming the rest of the data on this packet.
	} NET_USER_PACKET;
	
	typedef struct _PACKET_FORWARDED_OLD {
		SOCKADDR_STORAGE originSockAddr;
		NET_USER_PACKET netter;
		// The data following this is the forwarded packet data.
	} PACKET_FORWARDED_OLD;
	
	typedef struct _HUB_OUT_OF_BAND {
		uint32_t instanceId = 0;
		uint8_t portOffsetHBO = 0xFF;
		uint16_t portOriginalHBO = 0;
	} HUB_OUT_OF_BAND;
	
	typedef struct _HUB_RELAY {
		SOCKADDR_STORAGE destSockAddr;
		NET_USER_PACKET netterOrigin;
		// The data following this is the packet data.
	} HUB_RELAY;
	
#pragma pack(pop) // Return to original alignment setting.
}
