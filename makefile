CXX ?= g++
# -O0 Reduce compile time and make debugging produce expected results.
# -O3 Most Optimised.
CXXFLAGS ?= -g3 -O3

BIN_DIR = bin

TARGET_MAIN = xlln-hub
SRC_MAIN = main.cpp hub-server.cpp stats.cpp utils.cpp sha256.cpp

TARGET_TEST = unittests
SRC_TEST = tests.cpp utils.cpp sha256.cpp

# Phony targets to avoid conflicts with files named the same.
.PHONY: all clean $(BIN_DIR) $(TARGET_MAIN) $(TARGET_TEST)

# Default rule.
$(TARGET_MAIN): $(SRC_MAIN) | $(BIN_DIR)
	$(CXX) $(CXXFLAGS) -o "$(BIN_DIR)/$(TARGET_MAIN)" $(SRC_MAIN);

all: clean $(TARGET_MAIN) $(TARGET_TEST)

$(TARGET_TEST): $(SRC_TEST) | $(BIN_DIR)
	$(CXX) $(CXXFLAGS) -o "$(BIN_DIR)/$(TARGET_TEST)" $(SRC_TEST);

# Ensure the directory exists.
$(BIN_DIR):
	mkdir -p $(BIN_DIR);

# Rule to clean up generated files.
clean:
	rm -f "$(BIN_DIR)/$(TARGET_MAIN)" "$(BIN_DIR)/$(TARGET_TEST)";
