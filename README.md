# XLiveLessNess (XLLN)
Games for Windows LiveLessNess. A complete Games For Windows - LIVE<sup>TM</sup> (GFWL) rewrite.

# XLLN Online Hub Server (XLLN-Hub)
A self-hostable hub server for [XLiveLessNess](https://gitlab.com/GlitchyScripts/xlivelessness) used for rebroadcasting packets and host/session information to other listening/subscribed XLLN clients.

## Building From Source
You will need `make` and `g++` to build this program. Execute the `make` command in the root of the repository (which uses the `makefile` configuration) to build this program.

The following `make` targets exist:
- xlln-hub
  - The default target if none specified. Builds the main program.
- unittests
  - Builds a program that is used to verify the functionality of some code when run.
- clean
  - Deletes all generated files.
- all
  - Executes all of the targets including clean first.

You can swap the `g++` compiler out with another of your choice like `clang++` by specifying it in the `CXX` variable and also the flags to it in the `CXXFLAGS` variable like this example: `make all CXX=clang++ CXXFLAGS=-Wall`.
 
## Running The Program
Currently this program only supports Little-Endian CPU's and POSIX, UNIX, Linux or Cygwin environments. There is no support for Windows / CMD / Powershell although it probably works fine on WSL.

This program has several execution arguments. You can use the 'help' argument/flag to list all the ones the program supports. Currently these are the available options:
- `-H`, `/H`, `-help`, `--help`, `/help`, `/?`, `-?`, `--?`
  - Prints out information regarding the available execution arguments/flags.
  - All command flags/options are case insensitive, arguments to the flags such as file paths may be case sensitive.
- `-V`, `--version`
  - Prints out the version of the XLiveLessNess Hub server and exits.
- `-I=<>`, `--hostaddr=<address>`
  - Sets the local network socket address to host on (IPv4 or IPv6).
  - If not specified the program will bind to the 'any' address.
- `-P=<>`, `--hostport=<port>`
  - Sets the local network socket port to host on.
  - The default is `1100`.
- `-S=<>`, `--statsfile=<filepath>`
  - Sets the file to write stats to (this is optional).
  - This file must exist in advance of running this program, it does not matter if it is empty.

While the program is running, you may enter additional commands which include:
- `help`, `h`, `commands`, `/help`, `/h`, `-help`, `-h`, `--help`, `--h`, `?`, `/?`, `-?`. `--?`
  - Prints out information regarding the available commands.
  - All commands are case insensitive.
- `exit`, `q`, `quit`, CTRL+C, CTRL+D
  - Quits the program gracefully (the 2nd CTRL+C keystroke will kill the program).
- `version`
  - Prints out the version of this XLiveLessNess Hub server.
- `count`
  - The total number of XLLN instances connected.
- `versions`
  - The total number of each XLLN version that is connected.
- `titles`
  - The total number of each Title/game that is connected.
- `titleversions`
  - The total number of each Title/game and their version that are connected.

## License

Copyright (C) 2025 Glitchy Scripts.

This library is free software; you can redistribute it and/or modify it under the terms of exclusively the GNU General Public License version 2 as published by the Free Software Foundation.

This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License version 2 along with this library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
